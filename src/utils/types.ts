export type Product = {
    id: number,
    name: String,
    picturesLink: Array<String>
    description: String,
    price: String,
    quantity: number,
    orderedQuantity: number
}

export type ViewsElements = {
    "home": String,
    "cart": String,
    "wishlist": String,
    "checkout": String,
    "userAccount": String,
}
