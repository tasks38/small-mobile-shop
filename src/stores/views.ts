import { defineStore } from 'pinia';
import type { ViewsElements } from "@src/utils/types";

export const useViews = defineStore('views', {
    state: () => {
        return {
            views: ["home" as unknown as ViewsElements] as Array<ViewsElements>,
        }
    },
    getters: {
        getViews: (state) => {
            return state.views;
        }
    },
    actions: {
        setViewActive(view: ViewsElements) {
            this.views = [];
            this.views.push(view);
        },
    },
})
