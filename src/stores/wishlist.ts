import { defineStore } from 'pinia';
import type { Product } from "@src/utils/types";
import { useProducts } from './products';

export const useWishlist = defineStore('wishlist', {
    state: () => ({
        wishlist: [] as Array<Product>,
    }),
    getters: {
        getWishlist: (state) => {
            return state.wishlist;
        },
    },
    actions: {
        addToWishlist(id: number) {
            const products = useProducts();
            const product: Product | undefined = products.getProducts.find((product: Product) => product.id === id);
            const isProductInWishlist: Boolean = !!this.wishlist.find((product: Product) => product.id === id);
            if (product && !isProductInWishlist) {
                this.wishlist.push(product);
            }
        },
        removeFromWishlist(id: number) {
            const products = useProducts();
            const product: Product | undefined = products.getProducts.find((product: Product) => product.id === id);
            const isProductInWishlist: Boolean = !!this.wishlist.find((product: Product) => product.id === id);
            if (product && isProductInWishlist) {
                this.wishlist = this.wishlist.filter((product: Product) => product.id !== id);
            }
        },
        isProductInWishlist(id: number) {
            return !!this.wishlist.find((product: Product) => product.id === id);
        }
    },
})
