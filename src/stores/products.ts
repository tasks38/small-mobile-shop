import { defineStore } from 'pinia';
import type { Product } from "@src/utils/types";

export const useProducts = defineStore('products', {
    state: () => ({
        products: [] as Array<Product>,
    }),
    getters: {
        getProducts: (state) => {
            return state.products;
        },
    },
    actions: {
        fetchCharacters() {
            fetch("defaultData.json")
                .then((response) => response.json())
                .then((data) => {
                    this.products = Object.values(data.products);
                });
        },
    },
})
