import { defineStore } from 'pinia';
import type { Product } from "@src/utils/types";
import { useProducts } from './products';

export const useCart = defineStore('cart', {
    state: () => ({
        cart: [] as Array<Product>,
    }),
    getters: {
        getCart: (state) => state.cart,
    },
    actions: {
        addToCart(id: number) {
            const products = useProducts();
            const product: Product | undefined = products.getProducts.find((product: Product) => product.id === id);
            const isProductInCart: Boolean = !!this.cart.find((product: Product) => product.id === id);
            if (product && !isProductInCart) {
                product.orderedQuantity = 1;
                this.cart.push(product)
            }
        },
        removeFromCart(id: number) {
            const products = useProducts();
            const product: Product | undefined = products.getProducts.find((product: Product) => product.id === id);
            const isProductInCart: Boolean = !!this.cart.find((product: Product) => product.id === id);
            if (product && isProductInCart) {
                this.cart = this.cart.filter((product: Product) => product.id !== id);
            }
        },
        addQuantity(id: number | undefined, quantity: number) {
            const product = this.cart.find((product: Product) => product.id === id);
            if (product) {
                product.orderedQuantity = quantity;
            }
        }
    },
})
